/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Fuel extends Obj{
    int volume;
    public Fuel(int x, int y,int volume) {
        super('F', x, y);
        this.volume=volume;
    }
    
    public int fillFuel(){
        int vol=volume;
        symbol='-';
        volume=0;
        return vol;
        
    }   
}
