/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Robot extends Obj {

    private Tablemap map;
    int fuel;

    public Robot(int x, int y, char symbol, Tablemap map, int fuel) {
        super(symbol, x, y);
        this.map = map;
        this.fuel = fuel;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                checkfuel();               
                if (canWalk(x, y - 1)) {
                    y = y - 1;
                    reducefuel();
                } else {
                    
                    return false;
                }
                break;
            case 'S':
            case 's':
                checkfuel();   
                if (canWalk(x, y + 1)) {
                    y = y + 1;
                    reducefuel();
                } else {
                    
                    return false;
                }
                break;
            case 'E':
            case 'd':
                checkfuel();   
                if (canWalk(x + 1, y)) {
                    x = x + 1;
                    reducefuel();
                } else {
                    
                    return false;
                }
                break;
            case 'W':
            case 'a':
                checkfuel();   
                if (canWalk(x - 1, y - 1)) {
                    x = x - 1;
                    reducefuel();
                } else {

                    return false;
                }
                break;
            default:
                return false;

        }
        if (map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!!! (" + x + ", " + y + ")");

        }
        return true;
    }

    public void checkfuel() {
        
        int fuel=map.fillFuel(x, y);
        if (fuel>0){
            this.fuel+=fuel;
        }
    }

    private boolean canWalk(int x, int y) {
        return fuel > 0 && map.inMap(x, y) && !map.isTree(x, y);
    }
    

    private void reducefuel() {
        fuel--;
    }

    @Override
    public String toString() {
        return "fuel=" + fuel;
    }

}
