/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Obj {
    protected int x, y;
    protected char symbol;

    public Obj(char symbol,int x, int y) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getsymbol() {
        return symbol;
    }
    public boolean isOn(int x , int y){
        return this.x==x && this.y==y;
    }
}
