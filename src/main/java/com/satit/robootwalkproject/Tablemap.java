/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
class Tablemap {

    private int width, height;
    private Robot robot;
    private Bomb bomb;
    private Obj[] objects = new Obj[100];
    int objCount = 0;

    public Tablemap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addObj(Obj obj) {
        objects[objCount] = obj;
        objCount++;
        
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObj(robot);
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObj(bomb);
    }
    public char getSymbolOn(int x,int y){
        char symbol ='-';
        for(int o=0;o<objCount;o++){
            if (objects[o].isOn(x, y)){
                symbol= objects[o].getsymbol();
            }
            
        }
        return symbol;
    }
    public void showMap() {
        showTitle();
        System.out.println(robot);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print(getSymbolOn(x,y));
            }     
            System.out.println();
            }
        
            shownewline();
        
    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void shownewline() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }
    private void showObj(Obj obj){
        System.out.print(obj.getsymbol());
    }
    private void showbomb() {
        System.out.print(bomb.getsymbol());
    }

    private void showRobot() {
        System.out.print(robot.getsymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
    public boolean isTree(int x ,int y){
        for(int o=0;o<objCount;o++){
            if (objects[o] instanceof Tree && objects[o].isOn(x, y) ){
                return true;
            }
            
        }
        return false;
    }
    public int fillFuel(int x ,int y){
        for(int o=0;o<objCount;o++){
            if (objects[o] instanceof Fuel && objects[o].isOn(x, y) ){
                Fuel fuel =(Fuel) objects[o];
                return fuel.fillFuel();
            }
            
        }
        return 0;
    }    
}
