/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

import java.util.Scanner;

/**
 *
 * @author Satit Wapeetao
 */
public class Mainprogram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Tablemap map = new Tablemap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5, 5);
        map.addObj(new Tree(10, 10));
        map.addObj(new Tree(9, 10));
        map.addObj(new Tree(10, 9));
        map.addObj(new Tree(11, 10));
        map.addObj(new Tree(5, 10));
        map.addObj(new Tree(1, 10));
        map.addObj(new Tree(6, 10));
        map.addObj(new Tree(8, 9));
        map.addObj(new Tree(11, 15));
        map.addObj(new Tree(13, 15));
        map.addObj(new Fuel(0, 5, 20));
        map.addObj(new Fuel(10, 15, 20));
        map.addObj(new Fuel(15, 15, 20));
        map.addObj(new Fuel(17, 5, 20));
        map.setBomb(bomb);
        map.setRobot(robot);

        while (true) {
            map.showMap();
            char direction = Inputdirection(kb);
            if (direction == 'q') {
                ByeBye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void ByeBye() {
        System.out.println("Bye bro");
    }

    private static char Inputdirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
